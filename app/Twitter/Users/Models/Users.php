<?php

namespace App\Twitter\Users\Models;

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Twitter\Users\Models\Users.
 *
 * @property int $id
 * @property int $twitter_id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $screen_name
 * @property string|null $location
 * @property string|null $url
 * @property string|null $description
 * @property string $lang
 * @property int $followers_count
 * @property int $friends_count
 * @property int $listed_count
 * @property int $favourites_count
 * @property int $statuses_count
 * @property string $profile_banner_url
 * @property string $profile_image_url
 * @property string $profile_link_color
 * @property bool $protected
 * @property bool $verified
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Users extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'twitter_id',
        'email',
        'password',
        'name',
        'screen_name',
        'location',
        'url',
        'description',
        'lang',
        'followers_count',
        'friends_count',
        'listed_count',
        'favourites_count',
        'statuses_count',
        'profile_banner_url',
        'profile_image_url',
        'profile_link_color',
        'protected',
        'verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'protected' => 'boolean',
        'verified' => 'boolean',
    ];
}
