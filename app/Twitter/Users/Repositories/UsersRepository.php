<?php

namespace App\Twitter\Users\Repositories;

use App\Twitter\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param  Users  $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Get the user's data based on his screen name.
     *
     * @param  string  $screen_name
     * @return mixed
     */
    public function getUserByScreenName(string $screen_name)
    {
        return $this->users
            ->where('screen_name', $screen_name)
            ->firstOrFail();
    }
}
