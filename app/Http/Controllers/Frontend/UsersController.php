<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use App\Twitter\Users\Repositories\UsersRepository;

class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * UsersController constructor.
     * @param  UsersRepository  $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * Get the profile of the user based on his screen name.
     *
     * @param  string  $screen_name
     * @return Factory|View
     */
    public function profile(string $screen_name)
    {
        $user = $this->usersRepository->getUserByScreenName($screen_name);

        return view('users.profile')
            ->with('user', $user);
    }
}
