<?php

namespace App\Http\Controllers\Frontend\Auth;

use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Twitter\Users\Requests\LoginRequest;

class LoginController extends Controller
{
    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Shows the login form.
     *
     * @return Factory|View
     */
    public function showLogin()
    {
        return view('auth.login');
    }

    /**
     * Handles the login request.
     *
     * @param  LoginRequest  $request
     * @return RedirectResponse
     */
    public function handleLogin(LoginRequest $request)
    {
        $details = $request->only('email', 'password');

        if (auth()->attempt($details)) {
            return redirect()
                ->route('home');
        }

        return redirect()
            ->route('login')
            ->with('error', trans('errors.login_failed'));
    }

    /**
     * Handles logout request.
     *
     * @return RedirectResponse
     */
    public function logout()
    {
        auth()->logout();

        session()->flush();

        return redirect()
            ->route('login');
    }
}
