<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Twitter\Users\Models\Users;

$factory->define(Users::class, function (Faker $faker) {
    return [
        'twitter_id' => random_int(1, 1000),
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('password'),
        'name' => $faker->name,
        'screen_name' => $faker->userName,
        'location' => $faker->city,
        'url' => $faker->url,
        'description' => $faker->text,
        'lang' => $faker->languageCode,
        'followers_count' => random_int(0, 10),
        'friends_count' => random_int(0, 10),
        'listed_count' => random_int(0, 10),
        'favourites_count' => random_int(0, 10),
        'statuses_count' => random_int(0, 10),
        'profile_banner_url' => $faker->imageUrl(),
        'profile_image_url' => $faker->imageUrl(),
        'profile_link_color' => $faker->hexColor,
        'protected' => $faker->boolean,
        'verified' => $faker->boolean,
        'remember_token' => Str::random(10),
    ];
});
