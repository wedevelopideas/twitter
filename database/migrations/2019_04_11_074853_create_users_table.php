<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('twitter_id')->nullable()->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('name');
            $table->string('screen_name');
            $table->string('location')->nullable();
            $table->string('url')->nullable();
            $table->string('description')->nullable();
            $table->string('lang');
            $table->unsignedInteger('followers_count');
            $table->unsignedInteger('friends_count');
            $table->unsignedInteger('listed_count');
            $table->unsignedInteger('favourites_count');
            $table->unsignedInteger('statuses_count');
            $table->string('profile_banner_url');
            $table->string('profile_image_url');
            $table->string('profile_link_color');
            $table->boolean('protected');
            $table->boolean('verified');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
