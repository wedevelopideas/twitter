<?php

namespace Tests\Feature;

use Tests\TestCase;

class HomeTest extends TestCase
{
    /** @test */
    public function test_shows_home()
    {
        $response = $this->actingAs($this->user)->get(route('home'));
        $response->assertSuccessful();
    }
}
