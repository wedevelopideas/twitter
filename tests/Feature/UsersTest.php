<?php

namespace Tests\Feature;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_shows_profile()
    {
        $response = $this->actingAs($this->user)->get(route('user', ['screen_name' => $this->user->screen_name]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_fails_to_show_profile()
    {
        $response = $this->actingAs($this->user)->get(route('user', ['screen_name' => 'john-doe']));
        $response->assertNotFound();
    }
}
