<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Frontend'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', ['uses' => 'LoginController@showLogin', 'as' => 'login']);
        Route::post('login', ['uses' => 'LoginController@handleLogin']);
        Route::get('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });

    Route::get('home', ['uses' => 'HomeController@home', 'as' => 'home']);
    Route::get('{screen_name}', ['uses' => 'UsersController@profile', 'as' => 'user']);
});
