<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }

        .ui.menu .item img.logo {
            margin-right: 1.5em;
        }

        .main.container {
            margin-top: 7em;
            padding: 0 1em;
        }
@yield('styles')
    </style>
</head>
<body>
@auth
<div class="ui fixed inverted menu">
    <div class="ui container">
        <a href="{{ route('home') }}" class="icon item">
            <i class="large home icon"></i>
        </a>
        <div class="icon item">
            <i class="large hashtag icon"></i>
        </div>
        <div class="icon item">
            <i class="large bell outline icon"></i>
        </div>
        <div class="icon item">
            <i class="large envelope outline icon"></i>
        </div>
        <div class="right menu">
            <a href="{{ route('user', ['screen_name' => auth()->user()->screen_name]) }}" class="item">
                <img src="{{ auth()->user()->profile_image_url }}" alt="{{ auth()->user()->name }}" class="ui avatar image">
                <span>{{ auth()->user()->name }}</span>
            </a>
            <a href="{{ route('logout') }}" class="icon item">
                <i class="sign out icon"></i>
            </a>
        </div>
    </div>
</div>
@endauth
@guest
<div class="ui fixed inverted menu">
    <div class="ui container">
        <div class="item">
            <i class="large twitter blue icon"></i>
            {{ trans('common.home') }}
        </div>
        <div class="right menu">
            <a href="{{ route('login') }}" class="item">
                <i class="large sign in icon"></i>
                {{ trans('common.login') }}
            </a>
        </div>
    </div>
</div>
@endguest
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
</body>
</html>