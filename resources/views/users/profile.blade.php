@extends('layouts.frontend')
@section('title', 'Twitter')
@section('content')
    <div class="ui main container">
        <div class="ui stackable fluid grid">
            <div class="row">
                <div class="column">
                    <div class="ui medium circular image">
                        <img src="{{ $user->profile_image_url }}" alt="{{ $user->name }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h3 class="ui header">
                        {{ $user->name }}
                    </h3>
                    <h4 class="ui header">
                        {{ '@'.$user->screen_name }}
                    </h4>
                </div>
            </div>
        </div>
    </div>
@endsection