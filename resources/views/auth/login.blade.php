@extends('layouts.frontend')
@section('title', 'Twitter')
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui blue image header">
                <i class="twitter icon"></i>
                <span class="content">
                    Log in to Twitter
                </span>
            </h2>
            <form action="{{ route('login') }}" method="post" class="ui large form">
                @csrf

                <div class="ui stacked segment">
                    <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" autocomplete="off" placeholder="{{ trans('common.email') }}">
                        </div>
                    </div>
                    <div class="field{{ $errors->has('password') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="{{ trans('common.password') }}">
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large twitter submit button">{{ trans('common.login') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('styles')
    body {
    background-color: #DADADA;
    }
    body > .grid {
    height: 100%;
    }
    .image {
    margin-top: -100px;
    }
    .column {
    max-width: 450px;
    }
@endsection